package ru.kazakov.jse23;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IllegalAccessException {

        List<Object> personList = new ArrayList<>();
        personList.add(new Person("Sergey", "Kazakov", LocalDate.of(1994, 9, 21)));
        personList.add(new Person("Alexandr", "Popov", "popov_av5@nlmk.com"));
        System.out.println();
        for (List<Description> lists : ListDescription.getDescription(personList)) {
            for (Description description : lists) {
                System.out.println(description);
            }
            System.out.println();
        }
    }

}
