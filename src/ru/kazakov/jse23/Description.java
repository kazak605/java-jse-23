package ru.kazakov.jse23;

public class Description {

    String parameterName;

    String parameterTypeName;

    boolean hasValue;

    public Description(String parameterName, String parameterTypeName, boolean hasValue) {
        this.parameterName = parameterName;
        this.parameterTypeName = parameterTypeName;
        this.hasValue = hasValue;
    }

    @Override
    public String toString() {
        return "Description{" +
                "parameterName='" + parameterName + '\'' +
                ", parameterTypeName='" + parameterTypeName + '\'' +
                ", hasValue=" + hasValue +
                '}';
    }

}
