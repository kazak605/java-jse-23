package ru.kazakov.jse23;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ListDescription {

    public static List<List<Description>> getDescription(List<Object> objects) throws IllegalAccessException {
        List<List<Description>> result = new ArrayList<>();

        Class<?> firstClazz = objects.get(0).getClass();
        for (Object object : objects) {
            Class<?> currentClazz = object.getClass();
            if (currentClazz != firstClazz) {
                throw new IllegalArgumentException("Objects in the list are of different types.");
            }
            List<Description> fieldsList = new ArrayList<>();
            Field[] fields = currentClazz.getDeclaredFields();
            for (Field field : fields) {
                if (field.get(object) != null) {
                    fieldsList.add(new Description(field.getName(), field.getType().getName(), true));
                } else {
                    fieldsList.add(new Description(field.getName(), field.getType().getName(), false));
                }
            }
            result.add(fieldsList);
        }
        return result;
    }
}
